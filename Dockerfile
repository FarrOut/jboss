FROM farrout/jboss-base:7.2
LABEL maintainer="Greg Farr"

CMD ["./bin/standalone.sh"]
